Source: cellwriter
Section: gnome
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               libgtk2.0-dev (>= 2.8),
               libxtst-dev
Standards-Version: 4.5.0
Homepage: https://github.com/risujin/cellwriter
Vcs-Browser: https://salsa.debian.org/debian/cellwriter
Vcs-Git: https://salsa.debian.org/debian/cellwriter.git

Package: cellwriter
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         librsvg2-common
Description: grid-entry handwriting input panel
 CellWriter is a grid-entry natural handwriting input panel.
 As you write characters into the cells, your writing is instantly
 recognized at the character level. When you press 'Enter' on the panel,
 the input you entered is sent to the currently focused application as if
 typed on the keyboard.
 .
   * Writer-dependent, learns your handwriting for reliable recognition
   * Correcting preprocessor algorithms account for digitizer noise,
     differing stroke order, direction, and number of strokes
   * Unicode support enables you to write in your native language
