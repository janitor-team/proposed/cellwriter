cellwriter (1.3.6-2) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Submit, Repository.
  * Fix day-of-week for changelog entries 1.3.4-1, 1.3.3-1, 1.3.1-1,
    1.3.0-1.

  [ Sudip Mukherjee ]
  * Fix FTBFS with gcc-10. (Closes: #957075)
  * Update Standards-Version to 4.5.0
  * Update compat level to 13.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Mon, 18 May 2020 23:20:06 +0100

cellwriter (1.3.6-1) unstable; urgency=medium

  * QA upload.
  * Orphan package. See #911113
  * Drop unused Build-Depends on libgnome2-dev (Closes: #885672)
  * New upstream release
  * Update watchfile and homepage
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Add minimal debian/gbp.conf
  * Bump debhelper compat to 11
  * Drop obsolete debian/menu
  * Bump Standards-Version to 4.2.1
  * Skip running dh_auto_clean because of problems with the tarball

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 15 Oct 2018 16:04:11 -0400

cellwriter (1.3.5-1) unstable; urgency=low

  * New upstream release (Closes: #690214).
  * Switch to format 3.0 (quilt).
  * debian/control:
    + add Uploaders: field.
    + add dep on librsvg2-common. (Closes: #495976)
    + bump Standards-Version to 3.9.4
    + add Vcs-* tags.
    + drop cdbs build-dep.
    + bump debhelper build-dep to 9.
  * debian/compat: update accordingly.
  * debian/rules: convert to use dh only.
  * debian/docs: add.
  * debian/watch: add.
  * debian/copyright: update to DEP-5.
  * debian/patches/binutils-gold.patch: drop, useless.
  * debian/patches/fix-string-format-error.patch: drop, useless.
  * debian/patches/Encoding_entry_desktop_file.diff: add to fix .desktop
    validation error.

 -- Andrea Colangelo <warp10@ubuntu.com>  Mon, 08 Jul 2013 18:24:47 +0200

cellwriter (1.3.4-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS with binutils-gold": add patch from Ubuntu / Mahyuddin Susanto:
    + debian/patches/binutils-gold.patch:
      - fix FTBFS with ld --no-add-needed (LP: #696848)
    (Closes: #554047)
  * Fix "FTBFS: src/statusicon.c:215:17: error: format not a string
    literal and no format arguments [-Werror=format-security]":
    add patch fix-string-format-error.patch from Eric Alexander.
    (Closes: #643362)

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Nov 2011 14:40:26 +0100

cellwriter (1.3.4-1) unstable; urgency=low

  * New upstream release

 -- Michael Levin <risujin@risujin.org>  Mon, 09 Jun 2008 12:34:00 -0500

cellwriter (1.3.3-1) unstable; urgency=low

  * New upstream release

 -- Michael Levin <risujin@risujin.org>  Fri, 25 Jan 2008 12:25:00 -0500

cellwriter (1.3.2-1) unstable; urgency=low

  * New upstream release
  * Updated Standards-Version to 3.7.3

 -- Michael Levin <risujin@risujin.org>  Thu, 24 Jan 2008 16:18:00 -0500

cellwriter (1.3.1-1) unstable; urgency=low

  * New upstream release

 -- Michael Levin <risujin@risujin.org>  Tue, 30 Oct 2007 00:28:00 -0500

cellwriter (1.3.0-1) unstable; urgency=low

  * New upstream release
    - Fixes problems under dark GTK themes (Closes: #446322)

 -- Michael Levin <risujin@risujin.org>  Mon, 29 Oct 2007 22:49:00 -0500

cellwriter (1.2.4-1) unstable; urgency=low

  * New upstream release
  * keyevent.c: Fixed warnings

 -- Michael Levin <risujin@risujin.org>  Mon,  8 Oct 2007 10:46:00 -0500

cellwriter (1.2.3-1) unstable; urgency=low

  * New upstream release

 -- Michael Levin <risujin@risujin.org>  Thu, 27 Sep 2007 11:12:00 -0500

cellwriter (1.2.2-1) unstable; urgency=low

  * New upstream release

 -- Michael Levin <risujin@risujin.org>  Sun, 23 Sep 2007 10:45:21 -0500

cellwriter (1.1.0-1) unstable; urgency=low

  * Initial release (Closes: #441087, #441088)

 -- Michael Levin <risujin@risujin.org>  Thu,  6 Sep 2007 13:12:44 -0500
